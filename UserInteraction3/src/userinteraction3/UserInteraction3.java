
package userinteraction3;

import java.awt.Color;
import static java.awt.Color.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JPanel;
 class UserInteraction3 extends JPanel implements KeyListener {

     private Ellipse2D.Float bidak;
     private Ellipse2D.Float apel;
     private int x,y;
     private int x1,y1;
     private final static int SIZE = 20;
     private char[][] maze;
     
     public UserInteraction3() {
         addKeyListener(this);
         Random r = new Random();
         
         int rand = r.nextInt(30);
         
         this.x = 25;
         this.y = 0;
        this.x1 = 26;
        this.y1 = rand;
         this.bidak = new Ellipse2D.Float(x*SIZE,y*SIZE,SIZE,SIZE);
         this.apel = new Ellipse2D.Float(x1*SIZE,y1*SIZE,SIZE,SIZE);
         
        try{
             File f = new  File("maze.txt");
             Scanner sc = new Scanner(f);
             this.maze = new char[33][];
             for(int i=0; i<33; i++)
                 this.maze[i] = sc.nextLine().toCharArray();
         }
         catch(FileNotFoundException e){}
         
 } 
    
    
     
     @Override
     public void paintComponent(Graphics g){
         super.paintComponent(g);
         Graphics2D g2 = (Graphics2D) g ;
         g2.setColor(BLUE);
         g2.fill(bidak);
         g2.setColor(RED);
         g2.fill(apel);
         g2.setColor(BLACK);
         int x,y;
         for(y=0; y<this.maze.length; y++)
             for(x=0; x<this.maze[y].length; x++)
                 if(this.maze[y][x] == '@')
                     g2.fillRect(x*SIZE,y*SIZE,SIZE,SIZE);
         
         
     }
     
     
     
     public void keyTyped(KeyEvent e){}
     public void keyPressed(KeyEvent e){
        // System.out.println("keyPressed:          "+e);
        if(e.getKeyCode() == 38 && y>0 && this.maze[y-1][x]!='@')       y--;
        if(e.getKeyCode() == 40 && y<32 && this.maze[y+1][x]!='@')     y++;
        if(e.getKeyCode() == 37 && x>0 && this.maze[x-1][y]!='@')       x--;
        if(e.getKeyCode() == 39 && x<48 && this.maze[x+1][y]!='@')     x++;
        this.bidak.setFrame(x*SIZE, y*SIZE, SIZE, SIZE);
        repaint();
     }
     public void keyReleased(KeyEvent e){}
   
    
}
