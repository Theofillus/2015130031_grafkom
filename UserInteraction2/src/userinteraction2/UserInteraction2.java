/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinteraction2;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.*;
import javax.swing.JPanel;

/**
 *
 * @author Vaio
 */
class UserInteraction2 extends JPanel implements MouseListener, MouseMotionListener{

    private Rectangle block;
    private boolean flag;
    private int deltaX, deltaY;
    
    public UserInteraction2(){
        addMouseListener(this);
        addMouseMotionListener(this);
        this.block = new Rectangle(50, 50 , 50, 50);
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.fill(this.block);
    }
    
    public void mousePressed(MouseEvent e){
        int x = e.getX();
        int y = e.getY();
        if(this.block.contains(x,y)){
            this.deltaX = x-(int)this.block.getX();
            this.deltaY = y-(int)this.block.getY();
            this.flag = true;
            System.out.println(deltaX+" "+deltaY);
        }
    }
    
    public void mouseReleased(MouseEvent e){
            this.flag = false;
    
    }
    public void mouseDragged(MouseEvent e){
        if(flag==true){
            int x = e.getX();
            int y = e.getY();
            int newX = x-this.deltaX;
            int newY = y-this.deltaY;
            if((newX >= 0 && newY <=this.getWidth()-50) &&
                    (newY >= 0 && newY <=this.getHeight()-50))
            this.block.setLocation(newX, newY);
            repaint();
    } }
public void mouseClicked(MouseEvent e){}
public void mouseEntered(MouseEvent e){}
public void mouseExited(MouseEvent e){}
public void mouseMoved(MouseEvent e){}

}
