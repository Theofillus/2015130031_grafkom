
package animasi2;

import java.awt.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.util.Random;
import javax.swing.JPanel;
import javax.swing.Timer;


class Animasi2 extends JPanel implements ActionListener{
private Ellipse2D.Float bidak;
private Random r;
private Color color =  Color.BLACK;
private JPanel area =  new JPanel();
private int  x , y;
private int arahX, arahY;
private int deltaX, deltaY;
private final static int SIZE = 50;
private final static int SPEED = 10;
private Timer timer;

public Animasi2(){
        x=500; y=350;
        Random rnd = new Random();
        double sudut = rnd.nextDouble()*Math.PI/2.0;
        this.arahX = (rnd.nextInt(2) == 0)?-1:1;
        this.arahY = (rnd.nextInt(2) == 0)?-1:1;
        this.deltaX = (int) (Math.cos(sudut)*SPEED);
        this.deltaY = (int) (Math.sin(sudut)*SPEED);
        this.bidak = new Ellipse2D.Float(x,y,SIZE,SIZE);
        this.timer = new Timer(33, this);
        this.timer.start();
}

public Color getWarna(){
    r = new Random();
    return color = new Color((int)r.nextInt(255),(int)r.nextInt(255),(int)r.nextInt(255));
}

 @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        //g2.setColor(Color.RED);
        //g2.setColor(Color.YELLOW);
        g2.setColor(Color.BLUE);
        g2.fill(this.bidak);
    }
    
    
    
    public void actionPerformed(ActionEvent e){
        this.x += (deltaX * arahX);
        this.y += (deltaY * arahY);
       
        if(this.x < 0 || this.x > getWidth())
            arahX *= -1; getWarna();
        
        //if(this.x - SIZE> -1 || this.x - SIZE < getWidth())
          //  arahX *= +1;
        
        if(this.y < 0 || this.y > getHeight())
            arahY *= -1; getWarna();
        this.bidak.setFrame(x,y,SIZE,SIZE);
        
        
        repaint();
    }
    
}
