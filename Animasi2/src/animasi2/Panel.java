
package animasi2;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.event.*;

public class Panel {


    public static void main(String[] args) {
 
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                TampilkanGUI();
            }
        });
        }
    
    private static void TampilkanGUI(){
        JFrame f = new JFrame("");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(1000,500);
        f.add(new Animasi2());
        f.setVisible(true);
    }
    
}
